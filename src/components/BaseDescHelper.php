<?php
/**
 * @desc    描述采集相关函数基类
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/9
 * Time: 14:37
 */

namespace Biqu\gbac\components;


abstract class BaseDescHelper {

    public $controllerNameSpace;

    public $baseController = 'Biqu\gbac\controllers\GBaseController';

    /**
     * 获取控制器描述
     * @param $controllerName
     * @return null|string
     */
    protected function getControllerDesc($controllerName){
        $reflaction = new \ReflectionClass($controllerName);
        if(!$reflaction->isSubclassOf($this->baseController)) return NULL;
        return $this->parseControllerDesc($reflaction->getDocComment());
    }

    /**
     * 获取指定Controller下所有action的菜单描述
     * @param $controllerName
     * @return array
     */
    protected function getActionsArr($controllerName){
        $reflaction = new \ReflectionClass($controllerName);
        if(!$reflaction->isSubclassOf($this->baseController)) return NULL;
        $methods = $reflaction->getMethods(\ReflectionMethod::IS_PUBLIC);
        if(!$methods) return NULL;
        $actions = array();
        foreach($methods as $method){
            $funcName = $method->getName();
            if(0!==strpos($funcName, 'action')) continue;
            if($desc = $this->parseDesc($method->getDocComment())){
                $actions[] = ['actionId'=>$this->camelToDash(str_replace('action', '', $funcName)), 'menudesc'=>$desc];
            }
        }
        return $actions;
    }

    /**
     * 获取controllerNameSpace下的所有控制器类名
     * @return array
     */
    public function getControllerList($dir=null, $prefix=null){
        ($dir===null) && $dir = $this->getPathOfControllers();
        $arr = scandir($dir);
        $classes = array();
        if($arr){
            foreach($arr as $k=>$a){
                if(empty($a) || $a == '.' || $a == '..' || $a[0] == '.') continue;
                if (is_dir("{$dir}/{$a}")){
                    $classes = array_merge($classes, $this->getControllerList("{$dir}/{$a}", $prefix ? "{$prefix}\\{$a}": $a ));
                }else{
                    $info = pathinfo($a);
                    if(!$info) continue;
                    $classes[] = $prefix ? ($prefix . '\\' . $info['filename']) : $info['filename'];
                }
            }
        }
        return $classes;
    }

    protected function getControllerNameSpace(){
        if(is_null($this->controllerNameSpace)){
            $this->controllerNameSpace = \Yii::$app->controllerNamespace;
        }
        return $this->controllerNameSpace;
    }

    protected function getPathOfControllers(){
        $namespace = $this->getControllerNameSpace();
        return \Yii::getAlias('@'.str_replace('\\', '/', $namespace));
    }

    /**
     * 解析注释文档，提取@controllerdesc标注的控制器描述
     * @param $docComment
     * @return null
     */
    protected function parseControllerDesc($docComment){
        $matches = array();
        preg_match('/@controllerdesc\s+(.*)/i', $docComment, $matches);
        return $matches ? $matches[1] : NULL;
    }

    public function camelToDash($str){
        $array = preg_split("/(?=[A-Z]{1,})/", lcfirst($str));
        if ($array) {
            $array = array_map('strtolower', $array);
            return implode('-', $array);
        }
        return '';
    }

    abstract protected function parseDesc($docComment);
} 