<?php
/**
 * @desc    菜单描述相关函数
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/9
 * Time: 14:37
 */

namespace Biqu\gbac\components;

class MenuDescHelper extends BaseDescHelper {

    public function getMenuDescArr(){
        $controllers = $this->getControllerList();
        $namespace = $this->getControllerNameSpace();
        $menuArr = [];
        if($controllers){
            foreach($controllers as $controller){
                if (strpos($controller,'\\')!==FALSE){
                    $keys = explode('\\', $controller);
                    $keys[] = $this->camelToDash(str_ireplace('controller', '', array_pop($keys)));
                    $key = implode('/', $keys);
                }else{
                    $key = $this->camelToDash(str_ireplace('controller', '', $controller));
                }
                $controllerName = $namespace.'\\'.$controller;
                if($menudesc = $this->getControllerDesc($controllerName)){
                    $tmpArr = [
                        'controller'    => $controllerName,
                        'menudesc'      => $menudesc,
                        'actions'       => $this->getActionsArr($controllerName),
                    ];
                    //如果没有action，则不收录该控制器
                    if(!empty($tmpArr['actions']))
                        $menuArr[$key] = $tmpArr;
                }
            }
        }
        //取得系统配置controllerMap中声明的外部Controller
        if(!empty(\Yii::$app->controllerMap)){
            foreach(\Yii::$app->controllerMap as $key=>$controllerName){
                if($menudesc = $this->getControllerDesc($controllerName)){
                    $tmpArr = [
                        'controller'    => $controllerName,
                        'menudesc'      => $menudesc,
                        'actions'       => $this->getActionsArr($controllerName),
                    ];
                    if(!empty($tmpArr['actions']))
                        $menuArr[$key] = $tmpArr;
                }
            }
        }
        return $menuArr;
    }

    /**
     * 解析注释文档，提取@menudesc标注的菜单描述
     * @param $docComment
     * @return null
     */
    protected function parseDesc($docComment){
        $matches = array();
        preg_match('/@menudesc\s+(.*)/i', $docComment, $matches);
        return $matches ? $matches[1] : NULL;
    }
}