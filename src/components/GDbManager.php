<?php
/**
 * @desc    The descriptions of this file.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/31
 * Time: 10:36
 */

namespace Biqu\gbac\components;


use yii\rbac\DbManager;

class GDbManager extends DbManager {

    public function checkAccess($userId, $permissionName, $params = []){
        $permissions = $this->getPermissionsByUser($userId);
        if(array_key_exists($permissionName, $permissions)){
            if(!$this->executeRule($userId, $permissions[$permissionName], $params))
                return false;
            return true;
        }else{
            return false;
        }
    }

    /**
     * 获取指定用户的所有权限项
     * @param int|string $userId
     * @return array|\yii\rbac\Permission[]
     */
    public function getPermissionsByUser($userId){
        return $this->getPermissionsWithDependencies(parent::getPermissionsByUser($userId));
    }

    /**
     * 获取指定角色（用户组）的所有权限项
     * @param string $roleName
     * @return array|\yii\rbac\Permission[]
     */
    public function getPermissionsByRole($roleName){
        return $this->getPermissionsWithDependencies(parent::getPermissionsByRole($roleName));

    }

    /**
     * 遍历权限项数组整合进依赖权限项
     * @param $permissions
     * @return array
     */
    protected function getPermissionsWithDependencies($permissions){
        if($permissions){
            foreach($permissions as $p){
                $dependencies = $this->getDependencies($p, $permissions);
                if($dependencies)
                    $permissions = array_merge($dependencies, $permissions);
            }
        }
        return $permissions;
    }

    /**
     * 获取指定权限项的依赖项
     * @param $permission
     * @param $permissions
     * @return array
     */
    protected function getDependencies($permission, $permissions){
        $dependenciesArr = [];
        if(!empty($permission->data['dependency'])){
            $dependencies = is_array($permission->data['dependency']) ? $permission->data['dependency'] : explode(',', $permission->data['dependency']);
            if($dependencies){
                foreach($dependencies as $denp){
                    if(!isset($permissions[$denp])){
                        $dependenciesArr[$denp] = $this->getPermission($denp);
                    }
                }
            }
        }
        return $dependenciesArr;
    }


} 