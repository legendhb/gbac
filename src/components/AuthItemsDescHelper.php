<?php
/**
 * @desc    权限描述相关函数
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/9
 * Time: 14:37
 */

namespace Biqu\gbac\components;


class AuthItemsDescHelper extends BaseDescHelper {

    public function getAuthDescArr(){
        $controllers = $this->getControllerList();
        $namespace = $this->getControllerNameSpace();
        $menuArr = [];
        if($controllers){
            foreach($controllers as $controller){
                if (strpos($controller,'\\')!==FALSE){
                    $keys = explode('\\', $controller);
                    $keys[] = $this->camelToDash(str_ireplace('controller', '', array_pop($keys)));
                    $key = implode('/', $keys);
                }else{
                    $key = $this->camelToDash(str_ireplace('controller', '', $controller));
                }
                $controllerName = $namespace.'\\'.$controller;
                if($menudesc = $this->getControllerDesc($controllerName)){
                    $tmp = [
                        'controller'    => $controllerName,
                        'menudesc'      => $menudesc
                    ];
                    $tmp['actions'] = $this->getActionsArr($controllerName);
                    if(!empty($tmp['actions'])){
                        $menuArr[$key] = $tmp;
                    }
                }
            }
        }
        //取得系统配置controllerMap中声明的外部Controller
        if(!empty(\Yii::$app->controllerMap)){
            foreach(\Yii::$app->controllerMap as $key=>$controllerName){
                if($menudesc = $this->getControllerDesc($controllerName)){
                    $menuArr[$key] = [
                        'controller'=>$controllerName,
                        'menudesc'=>$menudesc
                    ];
                    $menuArr[$key]['actions'] = $this->getActionsArr($controllerName);
                }
            }
        }
        return $menuArr;
    }


    /**
     * 解析注释文档，提取@auth标注的权限描述
     * @param $docComment
     * @return null
     */
    protected function parseDesc($docComment){
        $matches = array();
        preg_match('/@auth\s+(.*)/i', $docComment, $matches);
        if(!$matches){
            return NULL;
        }
        $desc = $matches[1];
        if($desc[0] =='[' && $desc[strlen($desc)-1] == ']'){
            @eval('$desc = ' . $desc . ';');
        }else{
            $desc = ['desc'=>$desc, 'dependency'=>[]];
        }
        return $desc;
    }
}