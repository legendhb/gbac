<?php

namespace Biqu\gbac\assets;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the [boostrap-iconpicker](http://victor-valencia.github.io/bootstrap-iconpicker/)
 *
 * @author LegendHB<legendhb@gmail.com>
 */
class IconPickerAsset extends AssetBundle
{

    public $sourcePath = '@bower/fontawesome-iconpicker/dist';
    public $js = [
        'js/fontawesome-iconpicker.min.js',
    ];
    public $css = [
        'css/fontawesome-iconpicker.min.css',
    ];
}
