<?php
namespace Biqu\gbac\widgets;
/**
 * Class Menu
 * Theme menu widget.
 * @customized by LegendHB
 */
class Menu extends \dmstr\widgets\Menu
{
    /**
     * Marked whether the active item was found to avoid multiple selection.
     * @var bool
     */
    protected $activeItemFound = false;

    /**
     * Checks whether a menu item is active.
     * This is done by checking if [[route]] and [[params]] match that specified in the `url` option of the menu item.
     * When the `url` option of a menu item is specified in terms of an array, its first element is treated
     * as the route for the item and the rest of the elements are the associated parameters.
     * Only when its route and parameters match [[route]] and [[params]], respectively, will a menu item
     * be considered active.
     * @param array $item the menu item to be checked
     * @return boolean whether the menu item is active
     */
    protected function isItemActive($item){

        if ($this->activeItemFound){
            return false;
        }

        if (!empty($item['rule']) && is_array($item['rule'])){
            //URL规则中任意一条被匹配到,就被视为命中
            foreach ($item['rule'] as $ruleItem) {
                if (parent::isItemActive(['url' => $ruleItem])){
                    $this->activeItemFound = true;
                    return true;
                }
            }
        }

        if (parent::isItemActive($item)){
            $this->activeItemFound = true;
            return true;
        }
        return false;

    }
}
