<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel Biqu\gbac\models\AdminMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '菜单管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-menu-index">

    <p>
        <?= Html::a('新建菜单组', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            'sort',
            'menu_id',
            'menu_name',
            'menu_url:url',
            ['attribute'=>'p_id', 'value'=>function($model){return ($model->p_id == 0 ? '<i style="color:#999">顶级菜单</i>' : \Biqu\gbac\models\AdminMenuSearch::getTopLevelMenuLabels()[$model->p_id]);}, 'format'=>'raw'],
                        // 'menu_rule',
            // 'sort',
            // 'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'tree'=>function($url, $model, $key){
                        return Html::a("<span class='glyphicon glyphicon-check'></span>",
                            ['menu-items','id'=>$model->menu_id],
                            ['title'=>'勾选菜单项', 'class'=>'menu-items-btn', 'menu_name'=>$model->menu_name]);
                    },
                    'items'=>function($url, $model, $key){
                        return Html::a("<span class='glyphicon glyphicon-list'></span>",
                            ['sub-items','id'=>$model->menu_id],
                            ['title'=>'子项管理',]);
                    }
                ],
                'template'=>'{tree} {items} {update} {delete}'
            ],
        ],
    ]); ?>
</div>
<?php \common\extensions\artDialog\artDialogAsset::register($this);?>
<script type="text/javascript">
$(document).ready(function(){
    $('.menu-items-btn').click(function(){
        var title = $(this).attr('menu_name');
        var url = this.href;
        dialog.load('编辑“' + title + '”的菜单项', url, null, null, function(){
            var tree = $.jstree.reference('#menu_tree');
            var selected = tree.get_bottom_checked(true);
            var items = [];
            $.each(selected, function(i,n){items[i] = {desc:n['original']['desc'], route:n['id']};});
            $.ajax({
                url:url,
                data:{items:items},
                type:'post',
                success:function(html){
                    dialog.tips('保存成功！');
                },
                error:function(xhr){
                    dialog.alert(xhr.responseText);
                }
            });
        });
        return false;
    });
});
</script>