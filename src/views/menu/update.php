<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Biqu\gbac\models\AdminMenu */

$this->title = '编辑菜单#'.$model->menu_id;
$this->params['breadcrumbs'][] = ['label' => '菜单管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
