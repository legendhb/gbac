<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel Biqu\gbac\models\AdminMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = ['label' => '菜单组列表', 'url' => ['menu/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-menu-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            'sort',
            'menu_id',
            'menu_name',
            'menu_url',
            ['attribute'=>'menu_icon', 'value' => function($model){return Html::tag('i', '', ['class' => 'fa '.$model->menu_icon]);}, 'format' => 'raw'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>
</div>
<?php \common\extensions\artDialog\artDialogAsset::register($this);?>
<script type="text/javascript">
$(document).ready(function(){
    $('.menu-items-btn').click(function(){
        var title = $(this).attr('menu_name');
        var url = this.href;
        dialog.load('编辑“' + title + '”的菜单项', url, null, null, function(){
            var tree = $.jstree.reference('#menu_tree');
            var selected = tree.get_bottom_checked(true);
            var items = [];
            $.each(selected, function(i,n){items[i] = {desc:n['original']['desc'], route:n['id']};});
            $.ajax({
                url:url,
                data:{items:items},
                type:'post',
                success:function(html){
                    dialog.tips('保存成功！');
                },
                error:function(xhr){
                    dialog.alert(xhr.responseText);
                }
            });
        });
        return false;
    });
});
</script>