<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Biqu\gbac\models\AdminMenu */
/* @var $form yii\bootstrap\ActiveForm */
\Biqu\gbac\assets\IconPickerAsset::register($this);
?>

<div class="admin-menu-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-2',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ]
    ]); ?>

    <?= $form->field($model, 'menu_name')->textInput(['maxlength' => 50]) ?>
    <?php if(!$model->is_item):?>
    <?= $form->field($model, 'p_id')->dropDownList(\Biqu\gbac\models\AdminMenuSearch::getTopLevelMenuLabels(), ['prompt'=>' - 顶级分类 - ']) ?>
    <?= $form->field($model, 'menu_url')->textInput(['maxlength' => 255]) ?>
    <?php endif;?>
    <?= $form->field($model, 'menu_rule')->textarea(['maxlength' => 255]) ?>
    <?= $form->field($model, 'menu_icon', [
        'inputOptions'=>['class'=>'form-control', 'placeholder'=>'请选择菜单图标', 'data-input-search' => 'true'],
        'template'=>"{label}\n{beginWrapper}\n<div class='input-group' style='width:200px;'>{input}<i role='iconpicker' class='btn btn-default input-group-addon'></i></div>\n{hint}\n{error}\n{endWrapper}",

    ])->textInput(['maxlength'=>true]) ?>
    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\Biqu\gbac\models\AdminMenu::$STATUS_LABELS) ?>

    <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? '创建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
$(function(){
    $('#adminmenu-menu_icon').iconpicker({
        placement : 'right'
    });
});
</script>