<?php
/**
 * @desc    The descriptions of this file.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/11
 * Time: 11:57
 */
$assets = new \backend\assets\AppAsset();
?>
<div id="menu_tree"></div>
<link rel="stylesheet" href="<?php echo $assets->baseUrl;?>/js/jstree/themes/default/style.min.css"/>
<script type="text/javascript" src="<?php echo $assets->baseUrl;?>/js/jstree/jstree.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#menu_tree').jstree(
        {
            'core':{'data':<?php echo $menuJson;?>},
            'plugins':['checkbox']
        }
    );
});
</script>