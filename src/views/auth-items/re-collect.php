<?php
$colors = [
    'nochange'=>'gray',
    'updated'=>'blue',
    'inserted'=>'green',
    'deleted'=>'red',
];
?>
<?php if(!empty($outputs)):?>
<table>
    <?php foreach($outputs as $k=>$v):?>
    <tr>
        <td width="300px"><?php echo $k;?></td>
        <td style="color:<?php echo $colors[$v];?>"><?php echo $v;?></td>
    </tr>
    <?php endforeach;?>
</table>
<?php endif;?>