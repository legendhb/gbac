<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户组管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-groups-index">

    <p>
        <?= Html::a('添加用户组', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter'=>['class'=>\yii\i18n\Formatter::className(), 'datetimeFormat'=>'php:Y-m-d H:i:s'],
        'columns' => [
            ['attribute'=>'name', 'label'=>'组名称'],
            ['attribute'=>'description', 'label'=>'组描述'],
            ['label'=>'创建时间', 'attribute'=>'createdAt', 'format'=>'datetime'],
            ['label'=>'更新时间', 'attribute'=>'updatedAt', 'format'=>'datetime'],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view'=>function ($url, $model) {
                                if(0 !== strcasecmp($model->name, 'superusers'))
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['admin-auth-items/group-assign', 'name'=>$model->name], [
                                        'title' => '组权限管理',
                                        'data-pjax' => '0',
                                        'class'=>'auth-items-btn',
                                        'group_name' => $model->name
                                    ]);
                            },
                    'update'=>function ($url, $model, $key) {
                            if(0 !== strcasecmp($model->name, 'superusers'))
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('yii', 'Update'),
                                    'data-pjax' => '0',
                                ]);
                            },
                    'delete'=>function ($url, $model, $key) {
                            if(0 !== strcasecmp($model->name, 'superusers'))
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }

                ],

            ],
        ],
    ]); ?>
</div>
<?php \common\extensions\artDialog\artDialog::widget(['dryRun'=>true]);?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.auth-items-btn').click(function(){
            var title = $(this).attr('group_name');
            var url = this.href;
            dialog.load('编辑“' + title + '”的权限', url, function(dialogObj){
                dialogObj.width(500);
            }, null, function(){
                var tree = $.jstree.reference('#menu_tree');
                var selected = tree.get_bottom_checked(true);
                var items = [];
                $.each(selected, function(i,n){items[i] = {desc:n['original']['desc'], route:n['id']};});
                console.log(items);
                $.ajax({
                    url:url,
                    data:{items:items},
                    type:'post',
                    success:function(html){
                        dialog.tips('保存成功！');
                    },
                    error:function(xhr){
                        dialog.alert(xhr.responseText);
                    }
                });
            });
            return false;
        });
    });
</script>
