<?php

/* @var $this yii\web\View */
/* @var $model Biqu\gbac\models\AdminGroups */

$this->title = '修改用户组信息: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '用户组管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['index', 'id' => $model->name]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="admin-groups-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
