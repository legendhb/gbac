<?php

/* @var $this yii\web\View */
/* @var $model Biqu\gbac\models\AdminGroups */

$this->title = '新建用户组';
$this->params['breadcrumbs'][] = ['label' => '用户组管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-groups-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
