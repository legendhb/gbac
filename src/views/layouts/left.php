<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $logoUrl ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->realname?></p>
            </div>
        </div>
        <?php
        $menuExtObj = new \Biqu\gbac\models\AdminMenuExt();
        $menuItems = $menuExtObj->getMenuForLte();
        ?>
        <?= \Biqu\gbac\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
