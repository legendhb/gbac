<?php
use yii\helpers\Html;

if (Yii::$app->controller->id == 'login' || Yii::$app->user->isGuest) {
    //Use 'main-login' layout while access login page.
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
    Yii::$app->end();
}

/* @var $this \yii\web\View */
/* @var $content string */

if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);
\backend\assets\JsCookieAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
$logoPath = dirname(dirname(__FILE__)) .  '/static/logo.png';
Yii::$app->assetManager->publish($logoPath);
$logoUrl = Yii::$app->assetManager->getPublishedUrl($logoPath);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        form div.required label.control-label:after {
            content:" * ";
            color:red;
        }
    </style>
</head>
<?php
$skin = !empty($_COOKIE['admin-lte-skin']) ? $_COOKIE['admin-lte-skin'] : 'skin-blue';
?>
<body class="hold-transition <?=$skin?> sidebar-mini <?php if (!empty($_COOKIE['sidebar-toggle-state'])) echo $_COOKIE['sidebar-toggle-state']; ?>">
<?php $this->beginBody() ?>
<div class="wrapper">

    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset, 'logoUrl' => $logoUrl]
    ) ?>

    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset, 'logoUrl' => $logoUrl]
    )
    ?>

    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>
</body>
<script type="text/javascript">
$(function(){
    $('.sidebar-toggle').click(function () {
        var state = $('body').hasClass('sidebar-collapse') ? '' : 'sidebar-collapse';
        Cookies.set('sidebar-toggle-state', state, {expires:365});
    });
    $('#theme_select_box div').click(function () {
        var skin = $(this).attr('data-theme');
        var body = $('body');
        var lightSkin = skin + '-light';

        if (body.hasClass(skin)){
            body.removeClass(skin).addClass(lightSkin);
            Cookies.set('admin-lte-skin', lightSkin, {expires:365});
            return false;
        }else if(body.hasClass(lightSkin)){
            body.removeClass(lightSkin).addClass(skin);
            Cookies.set('admin-lte-skin', skin, {expires:365});
            return false;
        }

        var classes = body.attr('class').split(' ');
        $.each(classes, function(i, n){
            if (n.indexOf('skin-') === 0){
                body.removeClass(n);
            }
        });
        body.addClass(skin);
        Cookies.set('admin-lte-skin', skin, {expires:365});
        return false;
    });
    $('.search-from button[type=reset]').click(function(){
        var form = $(this).parents('form');
        //清除textarea/input/select的内容
        form.find('textarea').val('');
        form.find('input[type=text]').val('');
        form.find('select').val('');
        //清除checkbox的选择
        form.find('input[type=checkbox]').removeAttr('checked');
        //所有的radioList默认选中第一个
        var radioListNames = [];
        $.each(form.find('input[type=radio]'), function(i, ele){
            var name = $(ele).attr('name');
            if(-1 == $.inArray(name, radioListNames)){
                radioListNames.push(name);
            }
        });
        $.each(radioListNames, function(i, radioName){
            $('input[name="' + radioName + '"]')[0].checked = true;
        });
        return false;
    });
});
</script>
</html>
<?php $this->endPage() ?>