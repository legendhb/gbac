<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<style>
    #theme_select_box div{cursor: pointer; height:20px;}
</style>
<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">
<header class="main-header">

    <?= Html::a('<span class="logo-mini">' . Yii::$app->name . '</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">收缩导航</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $logoUrl ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?=Yii::$app->user->identity->realname?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $logoUrl ?>" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?=Yii::$app->user->identity->realname?>
                                <small><?=Yii::$app->user->identity->remark?></small>
                            </p>
                        </li>
                        <li class="user-body" style="padding: 0 15px;">
                            <div class="row" id="theme_select_box">
                                <div class="col-xs-2 text-center bg-blue" data-theme="skin-blue">
                                </div>
                                <div class="col-xs-2 text-center bg-green" data-theme="skin-green">
                                </div>
                                <div class="col-xs-2 text-center bg-purple" data-theme="skin-purple">
                                </div>
                                <div class="col-xs-2 text-center bg-black" data-theme="skin-black">
                                </div>
                                <div class="col-xs-2 text-center bg-yellow" data-theme="skin-yellow">
                                </div>
                                <div class="col-xs-2 text-center bg-red" data-theme="skin-red">
                                </div>

                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin-users/profile" class="btn btn-default btn-flat">个人资料</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    '登出',
                                    ['/login/logout'],
                                    ['data-method' => 'get', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
            </ul>
        </div>
    </nav>
</header>
