<?php

namespace Biqu\gbac\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AdminMenuSearch represents the model behind the search form about `backend\models\AdminMenu`.
 */
class AdminMenuSearch extends AdminMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'p_id', 'sort', 'status'], 'integer'],
            [['menu_name', 'controller', 'menu_url', 'menu_rule', 'is_item'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['sort'=>SORT_DESC]],
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'menu_id' => $this->menu_id,
            'p_id' => $this->p_id,
            'sort' => $this->sort,
            'status' => $this->status,
            'is_item' => $this->is_item,
        ]);

        $query->andFilterWhere(['like', 'menu_name', $this->menu_name])
            ->andFilterWhere(['like', 'controller', $this->controller])
            ->andFilterWhere(['like', 'menu_url', $this->menu_url])
            ->andFilterWhere(['like', 'menu_rule', $this->menu_rule]);
        return $dataProvider;
    }

    public static function getTopLevelMenusArr(){
        $menus = self::find()->where(['p_id'=>0])->orderBy('sort DESC')->all();
        $menusArr = [];
        if($menus)
            foreach($menus as $menu){
                $menusArr[$menu->menu_id] = $menu->attributes;
            }
        return $menusArr;

    }

    /**
     * 获取顶级菜单项标签数组
     */
    public static function getTopLevelMenuLabels(){
        $menus = self::getTopLevelMenusArr();
        $labelsArr = [];
        if($menus)
            foreach($menus as $menu)
                $labelsArr[$menu['menu_id']] = $menu['menu_name'];
        return $labelsArr;
    }


}
