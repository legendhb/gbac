<?php

namespace Biqu\gbac\models;

/**
 * This is the model class for table "admin_menu".
 *
 * @property integer $menu_id
 * @property integer $p_id
 * @property string $menu_name
 * @property string $controller
 * @property string $menu_url
 * @property string $menu_rule
 * @property string $menu_icon
 * @property integer $is_item
 * @property integer $sort
 * @property integer $status
 */
class AdminMenu extends \yii\db\ActiveRecord
{

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static $STATUS_LABELS = array(
        self::STATUS_ACTIVE => '激活',
        self::STATUS_DEACTIVE => '禁用',
    );

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	['menu_name', 'required'],
            [['p_id', 'sort', 'status', 'is_item'], 'integer'],
            [['menu_name', 'controller'], 'string', 'max' => 50],
            [['menu_url', 'menu_rule'], 'string', 'max' => 255],
            [['menu_icon'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => '菜单ID',
            'p_id' => '上级菜单',
            'menu_name' => '菜单名称',
            'controller' => '菜单所属控制器',
            'menu_url' => '菜单URL',
            'menu_rule' => '菜单规则',
            'menu_icon' => '菜单图标',
            'is_item' => '菜单子项',
            'sort' => '排序',
            'status' => '状态 ',
        ];
    }

    public function beforeSave($insert){
        //TODO：找出原因好吗- -#
        $this->p_id = (int)$this->p_id;
        return true;
    }

    /**
     * 根据提交的勾选菜单项更新数据
     * @param $p_id
     * @param array $submitItems
     */
    public function updateMenuItem($p_id, $submitItems=[]){
        $existsRoutes = $existsRouteItems = $submitRoutes = $submitMenuNames = [];

        //本次提交的路由
        if ($submitItems){
            foreach ($submitItems as $submitItem) {
                $submitRoutes[] = $submitItem['route'];
                $submitMenuNames[$submitItem['route']] = $submitItem['desc'];
            }
        }

        //当前存在的路由
        $existsItems = static::findAll(['p_id'=>$p_id, 'is_item'=>1]);

        if ($existsItems){
            foreach ($existsItems as $existsItem) {
                $route = str_replace(array('[', ']'), array('', ''), $existsItem->menu_url);
                if ($route){
                    $existsRoutes[] = $route;
                    $existsRouteItems[$route] = $existsItem;
                }
            }
        }

        //找到需要删除/增加的路由
        $itemsToDel = array_diff($existsRoutes, $submitRoutes);
        $itemsToAdd = array_diff($submitRoutes, $existsRoutes);

        if ($itemsToDel){
            foreach ($itemsToDel as $item) {
                isset($existsRouteItems[$item]) && $existsRouteItems[$item]->delete();
            }
        }

        if ($itemsToAdd){
            foreach ($itemsToAdd as $item) {
                $newItem = new AdminMenu();
                $newItem->loadDefaultValues();
                $newItem->attributes = [
                    'p_id'=>$p_id,
                    'menu_url'=>"[{$item}]",
                    'menu_name'=>$submitMenuNames[$item],
                    'is_item' => 1,
                ];
                $newItem->save();
            }
        }
    }

}
