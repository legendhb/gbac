<?php

namespace Biqu\gbac\models;

use Yii;
/**
 * This is the model class for table "admin_groups".
 *
 * @property string $name
 * @property string $description
 */
class AuthItems extends \yii\base\Model
{

    public $name;
    public $description;
    public $dependency;

    private $_role;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => '权限',
            'description' => '权限描述',
            'dependency' => '依赖权限',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * 给指定的角色批量赋权
     * @param $role
     * @param $permissionsArr
     */
    public function setPermissions($role, $permissionsArr){
        Yii::$app->authManager->removeChildren($role);
        if(is_array($permissionsArr) && !empty($permissionsArr)){
            foreach($permissionsArr as $permission){
                $permissionObj = Yii::$app->authManager->getPermission($permission['route']);
                Yii::$app->authManager->addChild($role, $permissionObj);
            }
        }
    }

    /**
     * 获取指定角色的权限项
     * @param $role
     */
    public function getPermissions($role){
        return Yii::$app->authManager->getPermissionsByRole($role->name);
    }


    /**
     * 设置用户所属组（所扮演角色）
     * @param $uid
     * @param $groupsArr
     */
    public function setUserGroups($uid, $groupsArr){
        $this->removeAssignments($uid);
        if(is_array($groupsArr) && !empty($groupsArr)){
            foreach($groupsArr as $roleName){
                $role = Yii::$app->authManager->getRole($roleName);
                if($role){
                    Yii::$app->authManager->assign($role, $uid);
                }
            }
        }
    }

    /**
     * 获取指定用户的所属组（所扮演角色）
     * @param $uid
     * @return array
     */
    public function getUserGroups($uid){
        $roles = Yii::$app->authManager->getRolesByUser($uid);
        $rolesArr = [];
        if($roles){
            foreach($roles as $role){
                $rolesArr[] = $role->name;
            }
        }
        return $rolesArr;
    }

    public static function allAuthItems(){
        return Yii::$app->authManager->getPermissions();
    }

    public static function allAuthItemsClassifiedByController(){
        $permissions = self::allAuthItems();
        $permissionsArr = [];
        if($permissions){
            foreach($permissions as $permission){
                //去掉最后一个/后的action，得到控制器名称
                $path = explode('/', $permission->name);
                unset($path[count($path)-1]);
                $controller = implode('/', $path);
                $permissionsArr[$controller]['desc'] = $permission->data['controllerDesc'];
                $permissionsArr[$controller]['class'] = $permission->data['controllerClass'];
                $permissionsArr[$controller]['actions'][] = $permission;
            }
        }
        return $permissionsArr;
    }

    public function removeAssignments($userId){
        if (empty($userId)) {
            return false;
        }
        return Yii::$app->db->createCommand()->delete(Yii::$app->authManager->assignmentTable, ['user_id'=>$userId])->execute();
    }

    /**
     * 获得指定用户的所有赋权项
     * @param $userId
     * @return bool|\yii\rbac\Permission[]
     */
    public function getAssignments($userId){
        if (empty($userId)) {
            return false;
        }
        if(Yii::$app->authManager->getAssignment('SuperUsers', $userId)){
            //有授权超级用户，直接返回所有权限
            $assignments = Yii::$app->authManager->getPermissions();
        }else{
            $assignments = Yii::$app->authManager->getPermissionsByUser($userId);
        }
        return $assignments;
    }
}
