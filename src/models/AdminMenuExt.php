<?php

namespace Biqu\gbac\models;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;

class AdminMenuExt extends AdminMenu
{
    public static $exceptionControllers = array('MyController', 'SiteController', 'SsoController');
    
    public function getMenuTree($level=false, $pid=0)
    {
        static $_opened = array();
        if ( $level>=0 || $level===false ) {
             
            $result = $this->getMenuArray($pid);
            $data = array();
            if (count($result)>0) {
                foreach ( $result as $k=>$v ) {
                    //build next level paramters
                    $_level = $level===false ? $level: $level-1;
                    $_pid = $v['menu_id'];
                    $v['actived'] = $hasActived = false;
                    if($pid > 0){
//                        if($this->routeCompare($v)){
//                            $v['actived'] = true;
//                            $_opened[] = $pid;
//                        }
                        //判断是否有菜单对应的权限
                        if(!$this->authCompare($v)) continue;

                    }
                    $children = $this->getMenuTree($_level, $_pid);

                    if (is_array($children) && count($children)>0) {
                        $tmp2 = array();
                        foreach ( $children as $sk=>$sv ) {
                            array_push($tmp2, $sv);
                        }
                        $v['sub_menu'] = $tmp2;
                        if(in_array($_pid, $_opened)) $v['actived'] = true;
                    } else {
                        $v['sub_menu'] = array();
                        if(!$v['menu_url'] && !$v['is_item']){
                            continue;
                        }
                    }
                    //not add menu when have no children on level 0
                    if ( $pid===0 && count($children)==0 ) {
                        continue;
                    } else {
                        $data[] = $v;
                    }
                }
                return $data;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * 获取整合好的提供给LTE使用的菜单数组
     * @return array
     */
    public function getMenuForLte(){
        $menuArr = $this->getMenuTree();
        $menuItems = $this->parseLteMenu($menuArr);
        return $menuItems;
    }

    /**
     * 递归将菜单树转为适合AdminLTE的菜单结构
     * @param $menuArr
     * @param int $level
     * @return array
     */
    protected function parseLteMenu($menuArr, $level=0){
        $menuItemsArr = [];
        $defaultIcons = [
            '', 'ellipsis-h', 'circle-o'
        ];
        foreach ($menuArr as $menuItem) {
            $tmp = [
                'label' => $menuItem['menu_name'],
                'icon' => (empty($menuItem['menu_icon']) ? $defaultIcons[$level] : $menuItem['menu_icon']),
                'rule' => $this->parseMenuRule($menuItem['menu_rule']),
            ];
            if ($level == 0){
                $tmp['options']['class'] = 'header';
            }else{
                $tmp['url'] = '#';
            }
            if (!empty($menuItem['menu_url'])){
                $tmp['url'] = $this->parseMenuUrlForLte($menuItem['menu_url']);
            }
            if (!empty($menuItem['sub_menu'])){
                $subMenuArr = $this->parseLteMenu($menuItem['sub_menu'], $level+1);
                if ($level == 0){
                    $menuItemsArr[] = $tmp;
                    $menuItemsArr = array_merge($menuItemsArr, $subMenuArr);
                }else{
                    $tmp['items'] = $subMenuArr;
                    $menuItemsArr[] = $tmp;
                }
            }else{
                $menuItemsArr[] = $tmp;
            }
        }
        return $menuItemsArr;
    }

    /**
     * 渲染菜单
     */
    public function renderMenuTree($menuArray = array(), $n=0, $expand=false, $ancId=0)
    {
        $html = '';
        if ($menuArray) {
            foreach ($menuArray as $k => $v) {
                $menuUrl = $this->parseMenuUrl($v['menu_url']);
                if ( empty($v['sub_menu']) ) {
                    if ( $v['actived'] ) {
                        $class = 'list-group-item active';
                    } else {
                        $class = 'list-group-item';
                    }
                    $html .= '<a class="'.$class.'" href="'.$menuUrl.'">' . $v['menu_name'] . "</a>\n";
                } else {
                    $aria_expanded = $v['actived'] ? ' aria-expanded="true"' : ' aria-expanded="false"';
                    $html .= '<a class="list-group-item" href="#sm-' . $v['p_id'] . '-'.$v['menu_id'].'" data-toggle="collapse" data-parent="#sm-' . $v['p_id'] . '"' . $aria_expanded . '>' . $v['menu_name'] . '<b class="caret"></b></a>' . "\n";
                    $html = $html . $this->renderMenuTree($v['sub_menu'], $v['menu_id'], $v['actived'], $v['p_id']);
                }
            }
            $in = $expand ? ' in' : '';
            $html = ($html && $n>0) ? '<div id="sm-' . $ancId . '-'.($n).'" class="submenu panel-collapse collapse' . $in . '" >' . $html . "</div>\n" : $html;
        }
        return $html;
         
    }


    public function parseMenuUrl($menu_url, $params=null){
        if(empty($menu_url)) return '';
        if($menu_url[0] == '[' && $menu_url[strlen($menu_url)-1] == ']'){
            //menu_url规则解析
            $menu_url = substr($menu_url, 1, -1);
            $routes = explode('/', $menu_url);
            if ($routes){
                foreach ($routes as &$route) {
                    $route = $this->camelToDash($route);
                }
            }
            $menu_url = implode('/', $routes);
            $r = [$menu_url];
            if($params) $r+=$params;
            return Yii::$app->urlManager->createUrl($r);
        }
        return $menu_url;
    }

    /**
     * 解析菜单匹配规则
     * @param $rule
     * @return bool
     */
    public function parseMenuRule($rule){
        $rule = trim($rule);
        if (empty($rule))
            return false;
        $ruleArr = explode("\r\n", $rule);
        $urlArr = [];
        if ($ruleArr){
            foreach ($ruleArr as $ruleItem) {
                $urlInfo = parse_url($ruleItem);
                if (!empty($urlInfo['path'])){
                    $tmp[0] = $urlInfo['path'];
                    if (!empty($urlInfo['query'])){
                        parse_str($urlInfo['query'], $queryInfo);
                        $tmp = array_merge($tmp, $queryInfo);
                    }
                    $urlArr[] = $tmp;
                }
            }
        }
        return $urlArr;

    }

    public function parseMenuUrlForLte($menu_url){
        if(empty($menu_url)) return '';
        if($menu_url[0] == '[' && $menu_url[strlen($menu_url)-1] == ']'){
            //menu_url规则解析
            $menu_url = substr($menu_url, 1, -1);
            $routes = explode('/', $menu_url);
            if ($routes){
                foreach ($routes as &$route) {
                    $route = $this->camelToDash($route);
                }
            }
            $menu_url = implode('/', $routes);
            $r = [$menu_url];
            return $r;
        }
        return $menu_url;
    }

    public function camelToDash($str){
        $array = preg_split("/(?=[A-Z]{1,})/", lcfirst($str));
        if ($array) {
            $array = array_map('strtolower', $array);
            return implode('-', $array);
        }
        return '';
    }

    public function routeCompare($menu_info){
        if(!empty($menu_info['menu_rule'])){
            //TODO：规则优先
        }elseif(!empty($menu_info['menu_url']) && $menu_info['menu_url'][0] == '[' && $menu_info['menu_url'][strlen($menu_info['menu_url'])-1] == ']'){
            $menu_url = substr($menu_info['menu_url'], 1, -1);
            list($controllerId, $actionId) = explode('/', $menu_url);
            if(0==strcasecmp($this->camelToDash($controllerId), Yii::$app->controller->id) && 0==strcasecmp($this->camelToDash($actionId), Yii::$app->controller->action->id)){
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 判断当前用户是否拥有指定的授权项
     * @param $menu_info
     * @return bool
     */
    public function authCompare($menu_info){
        if(!empty($menu_info['menu_url']) && $menu_info['menu_url'][0] == '[' && $menu_info['menu_url'][strlen($menu_info['menu_url'])-1] == ']'){
            $authArray = $this->getAuthArray();
            $allAuthArray = $this->getAllAuthArray();
            $menu_url = substr($menu_info['menu_url'], 1, -1);
            $routes = explode('/', $menu_url);
            if ($routes){
                foreach ($routes as &$route) {
                    $route = $this->camelToDash($route);
                }
            }
            $menu_url = implode('/', $routes);
            //不在权限控制里 或者 在权限控制里并且用户拥有权限
            !$authArray && $authArray = [];
            return !array_key_exists($menu_url, $allAuthArray) || array_key_exists($menu_url, $authArray);
        }
        return true;
    }
    
    public function getMenuOptions($level=false, $pid=0, $hideNotActive=false, $margin=0)
    {
        if ($level>=0 || $level===false) {
             
            $result = $this->getMenuArray($pid);
            if (count($result)>0) {
                $options = array();
                foreach ($result as $v) {
                    $key = (int)$v['menu_id'];
                    $options[$key] = str_repeat('|---', (int) $margin ) . $v['menu_name'];
                    $_level = $level===false? $level: $level-1;
                    $_pid = $v['menu_id'];
                    $tmp = $this->getMenuOptions($_level, $_pid, $hideNotActive, $margin+1);
                    if ( count($tmp) >0 ) {
                        $options = $options + $tmp;
                    }
                }
                return $options;
            }
            else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    public function getMenuArray($pid=0)
    {
        static $menuArray = array();
        if ( empty($menuArray) ) {
            $menuArray = $this->find()
            ->where(['status'=>1])
            ->orderBy('sort DESC')
            ->asArray()
            ->all();
        }
        $return = array();
        foreach ($menuArray as $menu)
        {
            if ($menu['p_id'] == $pid) {
                array_push($return, $menu);
            }
        }
        return $return;
    }

    /**
     * 获取当前用户的权限列表
     * @return mixed
     */
    public function getAuthArray(){
        static $authArray;
        if(NULL === $authArray){
            $uid = Yii::$app->user->getId();
            $authObj = new AuthItems();
            $authItems = $authObj->getAssignments($uid);
            if($authItems)
                foreach($authItems as $k=>$v)
                    $authArray[$k] = [
                        'description' => $v->description,
                        'ruleName' => $v->ruleName,
                        'data' => $v->data,
                    ];
        }
        return $authArray;
    }

    public function getAllAuthArray(){
        static $allAuthArray;
        if(NULL === $allAuthArray){
            $allAuthArray = AuthItems::allAuthItems();
        }
        return $allAuthArray;
    }
}
