<?php

namespace Biqu\gbac\models;

use Yii;
/**
 * This is the model class for table "admin_groups".
 *
 * @property string $name
 * @property string $description
 */
class AdminGroups extends \yii\base\Model
{

    public $name;
    public $description;

    private $_role;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 50],
        ];
    }

    public function scenarios(){
        return [
            'create' => ['name', 'description'],
            'default' => ['name', 'description'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => '组名称',
            'description' => '组描述',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function save($needValidation = true){
        if($needValidation && !$this->validate()){
            return $this->getErrors();
        }
        if($this->scenario == 'create'){
            $authManager = Yii::$app->authManager;
            $role = $authManager->createRole($this->name);
            $role->description = $this->description;
            $role->data = '';
            return $authManager->add($role);
        }else{
            $role = Yii::$app->authManager->getRole($this->_role->name);
            $role->name = $this->name;
            $role->description = $this->description;
            return Yii::$app->authManager->update($this->_role->name, $role);
        }
    }

    public function delete(){
        if(!$this->name){
            return false;
        }
        $role = Yii::$app->authManager->getRole($this->name);
        //TODO：判断该组下是否有成员，有的话阻止删除
        return Yii::$app->authManager->remove($role);
    }

    public static function loadGroup($name){
        $role = Yii::$app->authManager->getRole($name);
        if($role){
            $className = __CLASS__;
            $model = new $className;
            $model->attributes = [
                'name'=>$role->name,
                'description'=>$role->description,
            ];
            $model->_role = $role;
            return $model;
        }
        return NULL;
    }

    /**
     * 获取所有用户组（角色）
     * @return array
     */
    public static function allGroups(){
        $roles = Yii::$app->authManager->getRoles();
        $rolesArr = [];
        if($roles){
            foreach($roles as $name=>$role){
                $rolesArr[$name] = $role->name;
            }
        }
        return $rolesArr;
    }
}
