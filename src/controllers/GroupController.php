<?php

namespace Biqu\gbac\controllers;

use Biqu\gbac\components\AuthItemsDescHelper;
use Yii;
use Biqu\gbac\models\AdminGroups;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for AdminGroups model.
 * @controllerdesc    用户组管理模块
 */
class GroupController extends GBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdminGroups models.
     * @return mixed
     * @menudesc    用户组列表
     * @auth        用户组列表
     */
    public function actionIndex()
    {
        $groups = $this->authManager->getRoles();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $groups,
            'sort' => ['attributes'=>['name', 'createdAt', 'updatedAt']],
        ]);

        return $this->render('@gbac/views/group/index', [
            'dataProvider'=>$dataProvider,
        ]);
    }

    /**
     * Displays a single AdminGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@gbac/views/group/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdminGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @auth    新建用户组
     */
    public function actionCreate()
    {
        $model = new AdminGroups();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'name' => $model->name]);
        } else {
            return $this->render('@gbac/views/group/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdminGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @auth    更新用户组
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'name' => $model->name]);
        } else {
            return $this->render('@gbac/views/group/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdminGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @auth    删除用户组
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the AdminGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(0==strcasecmp($id, 'superusers'))
            throw new HttpException(403, 'SuperUsers was not allowed to be operated.');
        if (($model = AdminGroups::loadGroup($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
