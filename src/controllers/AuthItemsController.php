<?php

namespace Biqu\gbac\controllers;

use Biqu\gbac\components\AuthItemsDescHelper;
use Biqu\gbac\models\AuthItems;
use Yii;
use Biqu\gbac\models\AdminGroups;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for AdminGroups model.
 * @controllerdesc    权限管理模块
 */
class AuthItemsController extends GBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @menudesc    重新采集权限
     * @auth        重新采集权限
     */
    public function actionReCollect(){
        $authItemsOld = AuthItems::allAuthItems();
        $authItemKeysOld = !empty($authItemsOld) ? array_keys($authItemsOld) : array();        //取得旧的权限项列表

        $descObj = new AuthItemsDescHelper();
        $authItems = $descObj->getAuthDescArr();

        $authItemsSubmitted = $authItemKeysSubmitted = [];

        $outputs = [];          //输出内容

        if($authItems){
            foreach($authItems as $controller=>$v){
                if(!empty($v['actions'])){
                    foreach($v['actions'] as $action){
                        $name = "{$controller}/" . $descObj->camelToDash($action['actionId']);
                        $permission = $this->authManager->createPermission($name);
                        $permission->description = $action['menudesc']['desc'];
                        $permission->data = [
                            'controllerClass' => $v['controller'],
                            'controllerDesc' => $v['menudesc'],
                            'dependency' => !empty($action['menudesc']['dependency']) ? $action['menudesc']['dependency'] : NULL,
                        ];
                        $authItemsSubmitted[$name] = $permission;       //本次提交的权限项对象
                        $authItemKeysSubmitted[] = $name;               //本次提交的权限项列表
                        if(!array_key_exists($name, $authItemsOld)){
                            //本次提交的这条权限项之前没有，则先新增进去
                            $this->authManager->add($permission);
                            $outputs[$name] = 'inserted';
                        }
                    }
                }
            }
        }

        //取得之前有但是本次提交没有的权限项，进行删除操作
        $authItemsDiff = array_diff($authItemKeysOld, $authItemKeysSubmitted);
        if($authItemsDiff){
            foreach($authItemsDiff as $authItemName){
                $this->authManager->remove($authItemsOld[$authItemName]);
                $outputs[$authItemName] = 'deleted';
            }
        }

        //取得本次提交和之前存在的权限项交集，判断是否有更新
        $authItemsIntersect = array_intersect($authItemKeysOld, $authItemKeysSubmitted);
        if($authItemsIntersect){
            foreach($authItemsIntersect as $authItemName){
                $old = $authItemsOld[$authItemName];
                $new = $authItemsSubmitted[$authItemName];
                if($old->description!=$new->description || $old->ruleName!=$new->ruleName || $old->data!=$new->data){
                    //有需要更新的字段
                    $this->authManager->update($authItemName, $new);
                    $outputs[$authItemName] = 'updated';
                }else{
                    $outputs[$authItemName] = 'nochange';
                }
            }
        }

        return $this->render('@gbac/views/auth-items/re-collect', [
            'outputs' => $outputs,
        ]);

    }

    /**
     * 组权限编辑
     * @param string $name 组名称
     * @au3th    ['desc'=>'编辑组权限']
     */
    public function actionGroupAssign($name){
        $this->layout = false;
        $role = $this->authManager->getRole($name);
        if(!$role)
            throw new NotFoundHttpException('specified group not found.');

        $authItemsModel = new AuthItems();
        if(Yii::$app->request->isPost){
            //保存菜单项
            $items = Yii::$app->request->post('items');
            $authItemsModel->setPermissions($role, $items);
            Yii::$app->end();
        }

        $authItems = AuthItems::allAuthItemsClassifiedByController();
        $authItemsOwned = $authItemsModel->getPermissions($role);
        $authArr = [];
        foreach($authItems as $k=>$v){
            $tmp = [
                'id' => $k,
                'text' => $v['desc'] . " <i style='color:#999'>(" . $v['class'] . ")</i>",
                'desc' => $v['desc'],
                //                'state' => ['opened'=>false],
            ];
            if(!empty($v['actions'])){
                foreach($v['actions'] as $action){
                    $action = (array)$action;
                    list(, $actionId) = explode('/', $action['name']);
                    $childrenTmp = [
                        'id'=>$action['name'],
                        'text'=>$action['description'] . " <i style='color:#999'>(" . $actionId . ")</i>",
                        'desc' => $action['description'],
                        'dependency' => $action['data']['dependency'],
                    ];
                    if(array_key_exists($childrenTmp['id'], $authItemsOwned)){
                        $childrenTmp['state']['selected'] = true;
                    }
                    $tmp['children'][] = $childrenTmp;
                }
            }
            $authArr[] = $tmp;
        }

        return $this->render('@gbac/views/auth-items/group-assign', [
            'menuJson' => Json::encode($authArr),
        ]);

    }

    /**
     * Finds the AdminGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminGroups::loadGroup($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
