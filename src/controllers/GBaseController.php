<?php
/**
 * @desc    GBaseController class file(Group-Based-Access-Control).
 *          GBaseController must be extended by all of the applications controllers
 *          if the auto gbac should be used.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/9
 * Time: 11:56
 */

namespace Biqu\gbac\controllers;
use yii\web\Controller;
use yii\web\HttpException;

class GBaseController extends Controller {

    public $layout = 'main';

    /**
     * @var \yii\rbac\DbManager
     */
    public $authManager;

    public function beforeAction($action){
        if(!parent::beforeAction($action))
            return false;

        \Yii::$app->setComponents(['authManager'=>['class'=>'Biqu\gbac\components\GDbManager']]);
        \Yii::setAlias('@gbac', dirname(dirname(__FILE__)) . '/');
        $this->authManager = \Yii::$app->authManager;
        //权限控制
        if($assignment = $this->authManager->getAssignment('SuperUsers', \Yii::$app->user->id)){
            return true;
        }
        $route = "{$this->id}/{$action->id}";
        $permission = $this->authManager->getPermission($route);
        //TODO：dependency依赖关系处理
        if($permission && !\Yii::$app->user->can($route)){
            //没有声明@auth的方法也即未纳入权限控制
            if (\Yii::$app->user->isGuest) {
                \Yii::$app->user->loginRequired();
            } else {
                throw new HttpException(403, 'You are not allowed to access this page.');
            }
        }
        return true;
    }

} 