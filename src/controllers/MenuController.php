<?php
/**
 * @desc    MenuController class file.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/3/9
 * Time: 12:04
 */

namespace Biqu\gbac\controllers;
use Biqu\gbac\components\MenuDescHelper;
use Biqu\gbac\models\AdminMenu;
use Biqu\gbac\models\AdminMenuSearch;
use Yii;
use yii\helpers\Json;

/**
 * Class MenuController
 * @package Biqu\gbac\controllers
 * @controllerdesc    菜单管理
 */
class MenuController extends GBaseController {

    /**
     *
     * @menudesc    菜单列表
     * @auth        菜单列表
     */
    public function actionIndex(){
        $searchModel = new AdminMenuSearch();
        $params = \Yii::$app->request->queryParams;
        $params['AdminMenuSearch']['is_item'] = 0;
        $dataProvider = $searchModel->search($params);

        return $this->render('@gbac/views/menu/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new AdminMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @menudesc    新建菜单组
     * @auth        新建菜单组
     */
    public function actionCreate()
    {
        $model = new AdminMenu();
        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->menu_id]);
        } else {
            return $this->render('@gbac/views/menu/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdminMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @auth    编辑菜单组
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->is_item){
                return $this->redirect(['sub-items', 'id' => $model->p_id]);
            }
            return $this->redirect(['index', 'id' => $model->menu_id]);
        } else {
            return $this->render('@gbac/views/menu/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdminMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @auth    删除菜单组
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * 编辑菜单项
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @auth    菜单项分配
     */
    public function actionMenuItems($id){
        $this->layout = false;
        if(Yii::$app->request->isPost){
            //保存菜单项
            $menuModel = new AdminMenu();
            $items = Yii::$app->request->post('items');
            $menuModel->updateMenuItem($id, $items);
            Yii::$app->end();
        }

        $descObj = new MenuDescHelper();

        $itemsArr = array();
        if($items = AdminMenu::findAll(['p_id'=>$id, 'is_item'=>1])){
            //读取该菜单组下已有的菜单项
            foreach($items as $item){
                $key = str_replace(array('[', ']'), array('', ''), $item['menu_url']);
                $itemsArr[$key] = $item['menu_name'];
            }
        }
        $menu = $descObj->getMenuDescArr();
        $menuArr = [];
        foreach($menu as $k=>$v){
            $tmp = [
                'id' => $k,
                'text' => $v['menudesc'] . " <i style='color:#999'>(" . $v['controller'] . ")</i>",
                'desc' => $v['menudesc'],
                'state' => ['opened'=>true],
            ];
            if(!empty($v['actions'])){
                foreach($v['actions'] as $action){
                    $id = $k . '/' . $action['actionId'];
                    $childrenTmp = [
                        'id'=>$id,
                        'text'=>$action['menudesc'] . " <i style='color:#999'>(" . $action['actionId'] . ")</i>",
                        'desc' => $action['menudesc'],
                    ];
                    if(array_key_exists($childrenTmp['id'], $itemsArr)){
                        $childrenTmp['state']['selected'] = true;
                    }
                    $tmp['children'][] = $childrenTmp;
                }
            }
            $menuArr[] = $tmp;
        }
        return $this->render('@gbac/views/menu/menu_items', [
            'menuJson' => Json::encode($menuArr),
        ]);
    }

    /**
     * 子菜单项列表信息
     * @param $id
     * @return string
     */
    public function actionSubItems($id){

        $model = $this->findModel($id);

        $this->view->title = "“{$model->menu_name}”的菜单项";

        $searchModel = new AdminMenuSearch();
        $params = \Yii::$app->request->queryParams;
        $params['AdminMenuSearch'] = [
            'is_item' => 1,
            'p_id' => $id
        ];
        $dataProvider = $searchModel->search($params);

        return $this->render('@gbac/views/menu/sub-items', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * 升级菜单URL字段值，将action从驼峰转为短横线分隔
     */
    public function actionUpgrade(){
        $items = AdminMenu::find()->all();
        if ($items){
            $menuObj = new MenuDescHelper();
            foreach ($items as $item) {
                if (!empty($item->menu_url)){
                    if ($item->menu_url[0] == '[' && $item->menu_url[strlen($item->menu_url)-1] ==']'){
                        $menu_url = str_replace(array('[', ']'), array('', ''), $item->menu_url);
                        $routes = explode('/', $menu_url);
                        if ($routes){
                            foreach ($routes as &$route) {
                                $route = $menuObj->camelToDash($route);
                            }
                        }
                        $new_menu_url = implode('/', $routes);
                        if ($new_menu_url != $menu_url){
                            $item->menu_url = '[' . $new_menu_url . ']';
                            if($item->save()){
                                echo "[{$menu_url}]\t => \t {$item->menu_url}<br/>";
                            }
                        }
                    }
                }
            }
        }
        echo 'done!';
    }

    /**
     * Finds the AdminMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *
     * @#menudesc    刷新菜单项
     */
    public function actionRefreshMenu(){
//        $menuArr = MenuDescHelper::getMenuDescArr();
//        var_dump($menuArr);
    }
} 